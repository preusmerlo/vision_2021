
import random

def adivinar (intentos = 10):
    rand = random.randrange(0,101)
    while True:
        num  = int(input('Ingrese un numero:'))
        
        if (num == rand):
            print('Correcto, Ganaste')
            return
        else:
            intentos -= 1
            print('Incorrecto, te quedan ' + str(intentos) + ' intentos')

        if (intentos == 0):
            print('Perdiste el juego. El resultado correcto era ' + str(rand))
            return

adivinar(5)