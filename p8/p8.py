#! /bin/python3

######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: filename.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: day month. year
# Description 	: ...
######################################################################

######################################################################
#                               MODULES
######################################################################

import cv2
import numpy as np

######################################################################
#                               FUNCTIONS
######################################################################

######################################################################
#                           getPoints()
######################################################################

def getPoints (event, x , y , flags , param):
    global points,img, img_aux

    if event == cv2.EVENT_LBUTTONDOWN:

        if len(points) < 4:
            points.append([x,y])
        else:
            points.clear()
            points.append([x,y])
            img = img_aux.copy()
        
    elif event == cv2.EVENT_LBUTTONUP:
        cv2.circle(img,(x,y),5, (0,0,255), -1)

######################################################################
#                           orderPoints()
######################################################################

def orderPoints(points):
    mod = []
    # Calcula modulos
    for x,y in points:
        mod.append(np.sqrt(x**2 +y**2))

    # Arreglo de puntos ordenados
    new_points = [0,0,0,0]

    # Define puntos A y D
    new_points[0] = points.pop(mod.index(min(mod))    )     
    new_points[3] = points.pop(mod.index(max(mod)) - 1) 

    # Define puntos B y C
    if points[0][1] < points[1][1]:                     
        new_points[1] = points[0]
        new_points[2] = points[1]
    else:
        new_points[1] = points[1]
        new_points[2] = points[0]
    
    return new_points
    

######################################################################
#                           rectificar()
######################################################################

def rectificar(img, p_src):
    # p_src [A,B,C,D]
    p_src = np.float32(p_src)
    
    # Diferencia de puntos BA, DC, CA y DB
    difBA  = p_src [1] - p_src [0] 
    difDC  = p_src [3] - p_src [2]
    difCA  = p_src [2] - p_src [0]
    difDB  = p_src [3] - p_src [1]

    # Ancho y largo
    wAB  = np.sqrt(difBA[0]**2 + difBA[1]**2)
    wCD  = np.sqrt(difDC[0]**2 + difDC[1]**2)
    hAC  = np.sqrt(difCA[0]**2 + difCA[1]**2)
    hBD  = np.sqrt(difDB[0]**2 + difDB[1]**2)
    
    # Ancho y largo maximos
    W = max(int(wAB), int(wCD)) - 1
    H = max(int(hBD), int(hAC)) - 1

    # Puntos destino
    p_dst = np.float32([[0, 0 ],[W, 0 ],[0, H ],[W, H ]])
    M    = cv2.getPerspectiveTransform(p_src,p_dst)
    imga = cv2.warpPerspective(img,M,(W,H))
    return imga

######################################################################
#                               MAIN
######################################################################

img     = cv2.imread('cartas.jpg')
img_aux = cv2.imread('cartas.jpg')
draw    = False
points  = []

cv2.namedWindow('imagen')
cv2.setMouseCallback('imagen',getPoints)

while True:
    cv2.imshow('imagen',img)
    key = cv2.waitKey(1) 

    if key == ord('q'):
        break
    
    if key == ord('r'):
        img = cv2.imread('cartas.jpg')
    
    if key == ord('h'):
        img    = img_aux.copy()
        points = orderPoints(points)
        img    = rectificar(img, points)
        cv2.imwrite('output.jpg',img)

cv2.destroyAllWindows()
