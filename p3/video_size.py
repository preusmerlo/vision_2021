#!/usr/bin/python3
import sys
import cv2

#- Verificacion de la camara 
if (len(sys.argv) > 1):
    camera = sys.argv[1]
else:
    print('Indicar camara a usar')
    sys.exit(0)

# Abro la camara
cap    = cv2.VideoCapture(camera)
fps    = cap.get(cv2.CAP_PROP_FPS)

W = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH ))
H = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH ))

fourcc = cv2.VideoWriter_fourcc('X','V','I','D')
out    = cv2.VideoWriter('output.avi' , fourcc , fps , (W,H))

while(cap.isOpened()):
    ret,frame = cap.read()

    if ret:
        out.write(frame)
        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# Libera archivo
cap.release()

# Librea camara
out.release()

# Cierra ventanas
cv2.destroyAllWindows()


