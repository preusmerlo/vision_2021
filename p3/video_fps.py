import sys
import cv2

#- Verificacion de archivo de video
if (len(sys.argv) > 1):
    filename = sys.argv[1]
else:
    print('Indicar archivo de video')
    sys.exit(0)

# Captura de video
cap = cv2.VideoCapture(filename)

# Calculo de delay
fps = cap.get(cv2.CAP_PROP_FPS)
delay = int(1000/fps)

# Visualizacion
while(cap.isOpened()):
    ret,frame = cap.read()

    gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

    cv2.imshow('frame',gray)

    if((cv2.waitKey(delay) & 0xFF) == ord('q')):
        break

# Libera archivo
cap.release()
# Cierra ventanas
cv2.destroyAllWindows()

