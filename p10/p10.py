######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: aruco_creator.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 5 Oct. 2021
# Description 	: ...
######################################################################

######################################################################
#                               MODULES
######################################################################

import cv2 as cv
import numpy as np
import os
import shutil
from pygame import mixer

######################################################################
#                               MACROS
######################################################################

# Arucos ID
ID_UP_R = 7
ID_UP_L = 6
ID_DW_L = 8
ID_DW_R = 3

# Number of channels
MAX_CHANNEL = 4

# Window name
WIN_NAME = 'VIDEO'

# Audio and video synchronism constant
FRAMES_TO_DISCARD = 3

# Base video
BASE_VIDEO = 'base'

######################################################################
#                          OUTPUT DIRECTORY
######################################################################

if(os.path.exists('out')):
    shutil.rmtree('out')
if(not os.path.exists('out')):
    os.mkdir('out')

######################################################################
#                               FUNCTIONS
######################################################################

######################################################################
#                           find_arucos()
######################################################################

def find_arucos(img, draw = True):
    dictionary = cv.aruco.Dictionary_get(cv.aruco.DICT_6X6_250)
    parameters = cv.aruco.DetectorParameters_create()
    corners, ids,_ = cv.aruco.detectMarkers(img, dictionary,
                                                parameters=parameters)
    if draw:
        img = cv.aruco.drawDetectedMarkers(img, corners)
    return [corners, ids, img]
    
######################################################################
#                           homography()
######################################################################

def homography(corners, id, img, mask):
    
    # Return if 
    if any(id) == None or len(id) < 4:
        return img

    # Image and mask shape
    h_mask, w_mask = mask.shape[:2]
    h_img , w_img  =  img.shape[:2]
    
    # Reshape id (2D to 1D array)
    id = list(id.flatten())

    # Map aruco corners
    up_left  = corners[id.index(ID_UP_L)][0][0][:]
    up_rigth = corners[id.index(ID_UP_R)][0][0][:]
    dw_rigth = corners[id.index(ID_DW_R)][0][0][:]
    dw_left  = corners[id.index(ID_DW_L)][0][0][:]

    # Arucos and mask corners
    pts1 = np.array([up_left, up_rigth, dw_rigth, dw_left])
    pts2 = np.float32([ [0     ,      0], [w_mask, 0],
                        [w_mask, h_mask], [0, h_mask]])
    
    # Homography
    H = cv.getPerspectiveTransform(pts2, pts1)
    img_homo = cv.warpPerspective(mask, H, (w_img, h_img))
    
    cv.fillConvexPoly(img, pts1.astype(int), (0, 0, 0))
    img_out = img + img_homo
    return img_out

######################################################################
#                           menu()
######################################################################

def print_menu():
    m = '''
        \r\t\tMENU:
        \r e) Turn on /off tv
        \r w) Next channel
        \r s) Back channel
        \r q) Quit
    '''
    print(m)

######################################################################
#                           main()
######################################################################

def main():
    tv_state   = 'off'
    tv_channel = 'los_simpson'
    tv_counter = 0

    # Base video
    cap_base = cv.VideoCapture(f'video/{BASE_VIDEO}.mp4')
    fps_base = cap_base.get(cv.CAP_PROP_FPS)

    # Mask image
    mask_img_off = cv.imread("img/tv_off.jpeg")
    mask_img_on  = cv.imread("img/tv_aruco.jpeg")

    # Mask video
    cap_mask  = cv.VideoCapture(f'video/{tv_channel}.mp4')

    # Channel sound handler
    mixer.init()

    # Create new window
    cv.namedWindow(WIN_NAME, cv.WINDOW_AUTOSIZE)

    # Print menu
    print_menu()

    while cap_base.isOpened() and cap_mask.isOpened():

        # Decode menu
        key = str.lower(chr(cv.waitKey(33) & 0xFF))
        
        if key == 'q':
            break
        
        
        elif key == 'e':
            if tv_state == 'on':
                tv_state = 'off'
            else:
                tv_state = 'on' 
        
        elif key == 'w':
            if tv_counter == MAX_CHANNEL:
                tv_counter = 0
            else:
                tv_counter += 1
        
        elif key == 's':
            if tv_counter == 0:
                tv_counter = MAX_CHANNEL
            else:
                tv_counter -= 1

        # Turn on tv
        if tv_state == 'on' and key == 'e':
            # Mask video and sound
            cap_mask = cv.VideoCapture(f'video/{tv_channel}.mp4')
            mixer.music.load(f'sound/{tv_channel}.mp3')            
            mixer.music.play()

        # Turn off tv
        elif tv_state == 'off' and key == 'e':
                mixer.music.stop()

        # Set new channel
        if tv_state == 'on' and (key == 'w' or key == 's'):

            # Decode counter
            if tv_counter == 0:
                tv_channel = 'los_simpson'
            
            elif tv_counter == 1:
                tv_channel = 'terry_crews'
            
            elif tv_counter == 2:
                tv_channel = 'nueve_reinas'
            
            elif tv_counter == 3:
                tv_channel = 'rick_morty'
            
            elif tv_counter == 4:
                tv_channel = 'bad_boys'

            # Mask video and sound
            cap_mask = cv.VideoCapture(f'video/{tv_channel}.mp4')
            fps_mask = cap_mask.get(cv.CAP_PROP_FPS)
            mixer.music.load(f'sound/{tv_channel}.mp3')            
            mixer.music.play()

        # Get base frame
        _ , img  = cap_base.read()

        # Combine base, mask image and mask video
        # Detect real arucos
        corners, ids, img = find_arucos(img, draw = False)        
            
        if tv_state == 'on':
            # Add fake tv image
            img = homography(corners, ids, img, mask_img_on)
            
            # Detect fake arucos
            corners, ids, img = find_arucos(img, draw = False)

            # Get mask video frame
            # Discard some frames to synchronism audio
            for i in range(FRAMES_TO_DISCARD):
                _ , mask_frame = cap_mask.read()

            # Add fake tv video
            img = homography(corners, ids, img, mask_frame)
        else:
            # Add fake tv image
            img = homography(corners, ids, img, mask_img_off)

        cv.imshow(WIN_NAME, img)
        
    else:
        if not(cap_base.isOpened()):
            cap_base = cv.VideoCapture(f'video/{BASE_VIDEO}.mp4')
        
        if not(cap_mask.isOpened()):
            cap_mask   = cv.VideoCapture(f'video/{tv_channel}.mp4')

    cap_base.release()
    cv.destroyAllWindows()

######################################################################
#                        CALL MAIN FUNCTION
######################################################################

if __name__ == "__main__":
    main()