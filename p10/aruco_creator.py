######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: aruco_creator.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 5 Oct. 2021
# Description 	: ...
######################################################################

######################################################################
#                               MODULES
######################################################################

import os
import shutil
import cv2 as cv
import numpy as np

######################################################################
#                               MACROS
######################################################################

DEF_ID = 0
DEF_SIZE = 500

######################################################################
#                          OUTPUT DIRECTORY
######################################################################

if(os.path.exists('out')):
    shutil.rmtree('out')
if(not os.path.exists('out')):
    os.mkdir('out')

######################################################################
#                               FUNCTIONS
######################################################################

######################################################################
#                           create_aruco()
######################################################################

def create_aruco(id):
    img = np.zeros((DEF_SIZE, DEF_SIZE), dtype=np.uint8)
    dictionary = cv.aruco.Dictionary_get(cv.aruco.DICT_6X6_250)
    img = cv.aruco.drawMarker(dictionary, id, DEF_SIZE, img, 1)
    return img

######################################################################
#                           save_aruco()
######################################################################

def save_aruco(img, id):
    cv.imwrite(f'out/aruco_id_{id}.png', img)

######################################################################
#                           print_menu()
######################################################################

def print_menu():
    m = '''
        \r\t\tMENU:
        \r n) New aruco
        \r s) Save aruco
        \r q) Quit
    '''
    print(m)

######################################################################
#                           main()
######################################################################

def main():
    id = DEF_ID

    print_menu()
    img = create_aruco(id)

    while (True):
        
        # Show aruco
        cv.imshow('AURUCO', img)

        # Get key
        input_key = str.lower(chr(cv.waitKey(20) & 0xFF))
        
        # Save aruco
        if input_key == 's':
            save_aruco(img, id)
            print(f'Saved auruco - ID : {id}')

        # New aruco
        elif input_key == 'n':           
            if(id >= 249):
                id = 0
            else:
                id += 1
            img = create_aruco(id)

        # Close script
        if input_key == 'q':
            print('Bye bye')
            break

    # Close all windows
    cv.destroyAllWindows()

######################################################################
#                        CALL MAIN FUNCTION
######################################################################

if __name__ == "__main__":
    main()
