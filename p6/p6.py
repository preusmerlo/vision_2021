import cv2
import numpy as np
import math

draw    = False
ix , iy = -1 , -1
fx , fy = -1 , -1

def draw_rectangle (event, x , y , flags , param):
    global ix,iy,fx,fy,draw,img,img_aux

    if   event == cv2.EVENT_LBUTTONDOWN:
        draw   = True
        ix, iy = x , y

    elif event == cv2.EVENT_MOUSEMOVE:
        if draw is True:
            img = img_aux.copy()
            cv2.rectangle(img,(ix, iy),(x, y),(0,0,255), 2)
        
    elif event == cv2.EVENT_LBUTTONUP:
        draw   = False
        fx, fy = x , y
        
def similaridad(img, angle, tx, ty, s):
    M = np.float32([[ s * math.cos(angle * math.pi/180), s * math.sin(angle * math.pi/180),tx],
                    [-s * math.sin(angle * math.pi/180), s * math.cos(angle * math.pi/180),ty]])
    (rows, cols)= img.shape[:2]
    return cv2.warpAffine(img,M,(cols,rows))

img     = cv2.imread('hoja.jpg')
img_aux = cv2.imread('hoja.jpg')

angle   = 45
tx      = 10
ty      = 20
s       = 4

cv2.namedWindow('imagen')
cv2.setMouseCallback('imagen',draw_rectangle)

while True:
    cv2.imshow('imagen',img)
    key = cv2.waitKey(1) 

    if key == ord('q'):
        break
    
    if key == ord('r'):
        img = cv2.imread('hoja.jpg')
    
    if key == ord('s'):
        out = cv2.imread('hoja.jpg')[iy:fy, ix:fx]
        img = similaridad(out, angle, tx, ty, s)
        cv2.imwrite('output.jpg',img)

cv2.destroyAllWindows()
