import cv2

# Carga imagen
img = cv2.imread('hoja.jpg',cv2.IMREAD_GRAYSCALE)
limit = 200

# Guarda imagen original
cv2.imwrite('original.jpg', img)

# Copia imagen original
img_segmentada = img[:]

# Determina dimensiones de la imagen
x , y= img.shape

# Recorre la imagen y segmenta
for row in range(x):
    for col in range(y):
        if(img_segmentada[row,col] > limit):
            img_segmentada[row,col] = 255
        else:
            img_segmentada[row,col] = 0

# Muestra imagen modificada
cv2.imshow('Segmentada',img_segmentada)
cv2.waitKey(0)

# Guarda imagen segmentada
cv2.imwrite('segmentada.jpg', img_segmentada)





