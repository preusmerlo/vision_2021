#! /bin/python3

######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: filename.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: day month. year
# Description 	: ...
######################################################################

######################################################################
#                               MODULES
######################################################################

import cv2
import numpy as np

######################################################################
#                               FUNCTIONS
######################################################################

######################################################################
#                           transform()
######################################################################

def transform(img, p_src,W,H,scale):
    # p_src [A,B,C,D]
    p_src = np.float32(p_src)
        
    # Ancho y largo maximos
    W = W * scale
    H = H * scale

    # Puntos destino
    p_dst = np.float32([[0, 0 ],[W, 0 ],[0, H ],[W, H ]]) + p_src[0]
    M     = cv2.getPerspectiveTransform(p_src,p_dst)
   

    imga = cv2.warpPerspective(img,M,(1300,800))
    return imga

######################################################################
#                           measure()
######################################################################

def measure(event, x , y , flags , param):
    global x0,y0, img, img_rec_med,img_aux, draw, scale

    if event == cv2 .EVENT_LBUTTONDOWN:
        x0,y0 = x,y
        draw  = True

    elif event == cv2.EVENT_MOUSEMOVE and draw == True:
        img = img_rec_med.copy()
        cv2.circle(img, (x0, y0), 5, (100,255,0), -1)
        cv2.circle(img, (x, y  ), 5, (100,255,0), -1)
        cv2.line  (img, (x0, y0),(x, y), (145,255,140), 2)
        distance = (np.sqrt(( x0 - x) ** 2+ (y0 - y) **2 )) / 100
        text_c   = int((x0 + x) /2 + 15), int((y0 + y) / 2 - 15) 
        cv2.putText(img, "{:.2f}[m]" .format(distance/scale), (text_c), cv2.FONT_HERSHEY_TRIPLEX, 0.8, (0,0,255), 1)
    
    elif event == cv2.EVENT_LBUTTONUP:
        img_rec_med = img.copy()

        draw = False

######################################################################
#                               MAIN
######################################################################
img_name  = 'window.jpeg'
img       = cv2.imread(img_name)

draw      = False
points    = []
x0, y0    = 0,0

# Medidas de referencia en cm
W = 60      
H = 120

# Puntos de la rectificacion (porton)
points  = [[314, 16],[598, 54], [321, 534],[588, 500]]

# Cantidad de pixeles por cm
scale = 4

# Transformacion
img = transform(img, points,W,H,scale)
img_aux   = img.copy()
cv2.imwrite('transform.jpg',img)

cv2.namedWindow('imagen')

while True:
    cv2.imshow('imagen',img)
    key = cv2.waitKey(1) 

    if key == ord('q'):
        break
    
    elif key == ord('r'):
        img = img_aux.copy()
        img_rec_med = img.copy()

    elif key == ord('g'):
        cv2.imwrite('measure.jpg',img_rec_med)

    elif key == ord('h'):
        
        img_rec_med = img.copy()
        cv2.setMouseCallback('imagen', measure)

cv2.destroyAllWindows()