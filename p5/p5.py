import cv2
import numpy as np
import math

draw    = False
ix , iy = -1 , -1
fx , fy = -1 , -1

def draw_rectangle (event, x , y , flags , param):
    global ix,iy,fx,fy,draw,img,img_aux

    if   event == cv2.EVENT_LBUTTONDOWN:
        draw   = True
        ix, iy = x , y

    elif event == cv2.EVENT_MOUSEMOVE:
        if draw is True:
            img = img_aux.copy()
            cv2.rectangle(img,(ix, iy),(x, y),(0,0,255), 2)
        
    elif event == cv2.EVENT_LBUTTONUP:
        draw   = False
        fx, fy = x , y
        
def euclidiana(img, angle, tx, ty):
    M = np.float32([[ math.cos(angle * math.pi/180), math.sin(angle * math.pi/180),tx],
                    [-math.sin(angle * math.pi/180), math.cos(angle * math.pi/180),ty]])
    (rows, cols)= img.shape[:2]
    return cv2.warpAffine(img,M,(cols,rows))

img     = cv2.imread('hoja.jpg')
img_aux = cv2.imread('hoja.jpg')

angle   = 45
tx      = 10
ty      = 20

cv2.namedWindow('imagen')
cv2.setMouseCallback('imagen',draw_rectangle)

while True:
    cv2.imshow('imagen',img)
    key = cv2.waitKey(1) 

    if key == ord('q'):
        break
    
    if key == ord('r'):
        img = cv2.imread('hoja.jpg')
    
    if key == ord('e'):
        out = cv2.imread('hoja.jpg')[iy:fy, ix:fx]
        cv2.imwrite('output.jpg',euclidiana(out, angle, tx, ty))
        break

cv2.destroyAllWindows()
